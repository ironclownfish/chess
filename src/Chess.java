import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Menu;
import java.awt.MenuBar;
import java.awt.MenuItem;
import java.awt.MenuShortcut;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;

import javax.swing.ButtonGroup;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.KeyStroke;
import javax.swing.filechooser.FileNameExtensionFilter;

/*
 * 
 * 
 *  * Some edge cases for anyone making a chess engine:
 * - en passant
 * - Discovered check
 * - Discovered check due to en passant
 * - Check delivered by castling
 * - Check delivered by promotion
 * - Because of promotions, a given move does not have a well-defined outcome. 
 * 
 * * Notes
 * - A good way to test the legality checker is to put the AI in checkmate
 * and see if it finds a legal move.
 * 
 *  @author Nicholas Gyd�
 */

public class Chess extends JFrame implements ActionListener, ItemListener{	
	
	private Board board;
	public List<GameState> gameStates;
	public List<String> pgn;
	public List<String> cn;
	public Player whitePlayer;
	public Player blackPlayer;
	public boolean gameStopped;
	private byte currPly;
	private byte lineNum;
	private TextArea textArea;
	private InfoPanel sideData;
	private JMenuBar menuBar;
	private JMenu file;
		private JMenuItem newGame; private JMenuItem saveGame; private JMenuItem openGame;
		private JMenuItem openBook; private JMenuItem saveBook; private JMenuItem exportPGN; private JMenuItem quit;
	private JMenu options;
		private JCheckBoxMenuItem autoSwitchPOV;
		private JMenuItem flipBoard;
		private JRadioButtonMenuItem whitePlayerHuman; private JRadioButtonMenuItem whitePlayerAI;
		private JRadioButtonMenuItem blackPlayerHuman; private JRadioButtonMenuItem blackPlayerAI;
	private JMenu help;
	private Thread whitePlayerThread;
	private Thread blackPlayerThread;
	public Board[] lineBoards = new Board[]{ //DEBUG
			new Board(GameState.initialPosition, this),
			new Board(GameState.initialPosition, this),
			new Board(GameState.initialPosition, this),
			new Board(GameState.initialPosition, this),
			new Board(GameState.initialPosition, this),
			new Board(GameState.initialPosition, this)
	};
	
	private static final char[] pgnPieces = new char[]{'P', 'R', 'N', 'B', 'Q', 'K'};
	private static final char[] colNames  = new char[]{'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'};
	public boolean whiteIsHuman;
	public boolean blackIsHuman;
	
	public static void main(String[] args){
		Chess game = new Chess();
	}
	
	public Chess(){
		super("Chess engine");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		currPly = 0; lineNum = 0;
		
		gameStates = Collections.synchronizedList(new ArrayList<GameState>());
		gameStates.add(GameState.initialPosition);
		pgn = Collections.synchronizedList(new ArrayList<String>());
		cn = Collections.synchronizedList(new ArrayList<String>());
		board = new Board(GameState.initialPosition, this);
		setLayout(new GridBagLayout());
		GridBagConstraints bagCon = new GridBagConstraints();
		textArea = new TextArea(); textArea.setMinimumSize(new Dimension(30, 30));
		textArea.setPreferredSize(new Dimension(200, 320));
		sideData = new InfoPanel(this);
		sideData.setPreferredSize(new Dimension(200, 320));
		
		/////MENU BAR/////
		menuBar = new JMenuBar();
		file = new JMenu("File");
			newGame = new JMenuItem("New Game");		newGame.addActionListener(this);
			openGame = new JMenuItem("Open Game");  openGame.addActionListener(this);
			saveGame = new JMenuItem("Save Game"); 	saveGame.addActionListener(this);
			exportPGN = new JMenuItem("Export PGN");	exportPGN.addActionListener(this);
			openBook = new JMenuItem("Open Book");	openBook.addActionListener(this);
			saveBook = new JMenuItem("Save Book");	saveBook.addActionListener(this);
			quit = new JMenuItem("Quit");			quit.addActionListener(this);
			file.add(newGame); file.add(openGame); file.add(saveGame); file.addSeparator(); file.add(exportPGN);
			file.addSeparator(); file.add(openBook); file.add(saveBook); file.addSeparator(); file.add(quit);
			
		options = new JMenu("Options");
			flipBoard = new JMenuItem("Flip Board.");
			flipBoard.addActionListener(this);
			flipBoard.setAccelerator(KeyStroke.getKeyStroke('F', KeyEvent.CTRL_DOWN_MASK)); // set a keyboard shortcut
			options.add(flipBoard);
			
			autoSwitchPOV = new JCheckBoxMenuItem("Automatically flip board.");
			autoSwitchPOV.setSelected(false);
			autoSwitchPOV.addItemListener(this);
			options.add(autoSwitchPOV);
	
			options.addSeparator();
			ButtonGroup group = new ButtonGroup();
			whitePlayerHuman = new JRadioButtonMenuItem("White Player Human");
			whitePlayerHuman.addActionListener(this);
			whitePlayerHuman.setSelected(true);
			group.add(whitePlayerHuman);
			options.add(whitePlayerHuman);
	
			whitePlayerAI = new JRadioButtonMenuItem("White Player AI 1.0");
			whitePlayerAI.addActionListener(this);
			group.add(whitePlayerAI);
			options.add(whitePlayerAI);
		//---
			options.addSeparator();
			ButtonGroup group2 = new ButtonGroup();
			blackPlayerHuman = new JRadioButtonMenuItem("Black Player Human");
			blackPlayerHuman.addActionListener(this);
			blackPlayerHuman.setSelected(true);
			group2.add(blackPlayerHuman);
			options.add(blackPlayerHuman);
	
			blackPlayerAI = new JRadioButtonMenuItem("Black Player AI 1.0");
			blackPlayerAI.addActionListener(this);
			group2.add(blackPlayerAI);
			options.add(blackPlayerAI);
		//---
		help = new JMenu("Help");
		menuBar.add(file); menuBar.add(options); menuBar.add(help);
		setJMenuBar(menuBar);
		///////////////
		
		bagCon.weightx = 640; bagCon.weighty = 640;
		bagCon.gridheight = 2;
		add(board, bagCon);
		bagCon.weightx = 200; bagCon.weighty = 320;
		bagCon.gridx = 1; bagCon.gridheight = 1;
		add(textArea, bagCon);
		bagCon.gridy = 1;
		add(sideData, bagCon);
		
		
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
		
		whiteIsHuman = true;
		blackIsHuman = true;
		whitePlayer = whiteIsHuman?new HumanPlayer():new AIPlayer(this, true);
		blackPlayer = blackIsHuman?new HumanPlayer():new AIPlayer(this, false);
		startGame();
		
		/*//DEBUG
		JFrame inMinds = new JFrame("AI Brain");
		inMinds.setLayout(new GridLayout());
		for (int i = 0; i < lineBoards.length; ++i) {
			Board b = lineBoards[lineBoards.length - i - 1];
			b.setPreferredSize(new Dimension(190,190));
			inMinds.add(b);
		}
		inMinds.pack();
		inMinds.setVisible(true);*/
	}
	
	/*
	 * Returns true if the ply was accepted.
	 */
	public synchronized boolean doIfLegal(int sq1, int sq2){
		GameState currState = gameStates.get(gameStates.size()-1);
		if (currState.legalMove(sq1, sq2)) {
			GameState newState = currState.afterMove(sq1, sq2, false, this)[0];
			updateRepetition(newState);
			gameStates.add(newState);
			board.setGameState(newState);
			board.movSquare1 = sq1;
			board.movSquare2 = sq2;
			addNewPgn(currState, sq1, sq2, newState);
			addNewCn(sq1, sq2);
			++currPly;
			sideData.addCapturedPiece(currState.pieceType(sq2), currState.isWhite(sq2));
			textArea.append(pgn.get(pgn.size()-1));
			if (newState.isCheckmate()) {conveyMessage("\nCheckmate. " + (currState.whiteToMove()?"White ":"Black ") + "wins."); stopGame();}
			if (newState.isStalemate()) {conveyMessage("\nStalemate."); stopGame();}
			if (newState.repetitons() > 2) {conveyMessage("\nDraw by repetition."); stopGame();}
			if (!gameStopped) board.flipIfAuto();
			//if (newState.whiteToMove())
			return true;
		}
		return false;
	}
	
	private void addNewCn(int sq1, int sq2) {
		char z   = (char)('a' + (sq1%8));
		char zz  = (char)('0' + (sq1/8) + 1);
		char zzz = (char)('a' + (sq2%8));
		char zzzz= (char)('0' + (sq2/8) + 1);
		String zed = z + "" + zz + "" + zzz +  "" + zzzz;
		cn.add(zed);
	}

	public synchronized boolean doIfLegal(String s1s2) {
		int sq1 = (int)(s1s2.charAt(0) - 'a') + (int)(s1s2.charAt(1) - '0');
		int sq2 = (int)(s1s2.charAt(2) - 'a') + (int)(s1s2.charAt(3) - '0');
		return doIfLegal(sq1, sq2);
	}
	
	private void addNewPgn(GameState bef, int sq1, int sq2, GameState aft) {
		String str = "";
		if (bef.whiteToMove()) str += "\n" + Integer.toString(gameStates.size()/2) + ".";
		str += " ";
		//If en passant, do special case.
		if (bef.pieceType(sq1) == GameState.PAWN && Math.abs(sq2-sq1)%2 == 1 && bef.pieceType(sq2) >= GameState.BLANK0){
			pgn.add(str+ colNames[sq1%8] + 'x' + colNames[sq2%8] + Integer.toString(sq1/8 + 1));
			return;
		//If castling, do special case.
		} else if (bef.pieceType(sq1) == GameState.KING && Math.abs(sq1-sq2) == 2) {
			if (sq2 - sq1 == 2) {pgn.add(str+"O-O"); return;}
			if (sq1 - sq2 == 2) {pgn.add(str+"O-O-O"); return;}
		} else {
			char p1 = pgnPieces[bef.pieceType(sq1)];
			if (p1 != 'P') str += p1;
			//Add character to resolve knight ambiguity if necessary.
			if (bef.pieceType(sq1) == GameState.KNIGHT && 
				   (((aft.pieceType(sq2+6) == GameState.KNIGHT) && aft.isWhite(sq2+6) ==bef.whiteToMove())
				||	((aft.pieceType(sq2+10)== GameState.KNIGHT) && aft.isWhite(sq2+10)==bef.whiteToMove())
				||	((aft.pieceType(sq2+15)== GameState.KNIGHT) && aft.isWhite(sq2+15)==bef.whiteToMove())
				||	((aft.pieceType(sq2+17)== GameState.KNIGHT) && aft.isWhite(sq2+17)==bef.whiteToMove())
				||  ((aft.pieceType(sq2-6) == GameState.KNIGHT) && aft.isWhite(sq2-6) ==bef.whiteToMove())
				||	((aft.pieceType(sq2-10)== GameState.KNIGHT) && aft.isWhite(sq2-10)==bef.whiteToMove())
				||	((aft.pieceType(sq2-15)== GameState.KNIGHT) && aft.isWhite(sq2-15)==bef.whiteToMove())
				||	((aft.pieceType(sq2-17)== GameState.KNIGHT) && aft.isWhite(sq2-17)==bef.whiteToMove())))
					str += colNames[sq1%8];
			//Add character to resolve rook ambiguity if necessary
			if (bef.pieceType(sq1) == GameState.ROOK) {
				for (byte square = 0; square < 64; ++square) {
					if (aft.pieceType(square) == GameState.ROOK && aft.isWhite(square) == bef.whiteToMove()
							&& bef.legalMove(square, sq2)) {
						str += colNames[sq1%8];
					}
				}
			}
			
			if (bef.pieceType(sq2) < GameState.BLANK0) {
				if (p1 == 'P') str += colNames[sq1%8];
				str += 'x';
			}
			str += colNames[sq2%8] + Integer.toString(1 + sq2/8);
			//pawn promotion
			if (p1 == 'P' && sq2/8 == (bef.whiteToMove()?7:0)) {
				str += "=" + pgnPieces[aft.pieceType(sq2)];
			}
		}
		boolean checkmate = aft.isCheckmate();
		boolean check = aft.moverInCheck();
		if (check && !checkmate) str+='+';
		if (checkmate) str+='#';
		pgn.add(str);
	}
	
	public void updateRepetition(GameState gs){
		int repetitions = 0;
		GameState historicGS;
		for (int k = gameStates.size()-1; k > -1; --k) {
			historicGS = gameStates.get(k);
			if (gs.positionEquals(historicGS)) {
				repetitions = historicGS.repetitons()+1;
				break;
			}
		}
		int info = gs.hiddenInfo();
		info &= 0xffff3fff;
		info += repetitions<<14;
		gs.hideInfo(info);
	}
	
	public synchronized GameState currentState(){
		return gameStates.get(gameStates.size()-1);
	}
	
	public void resign(Player p){
		conveyMessage("\n" + (p.equals(whitePlayer)?"White ":"Black ") + "has resigned.");
		stopGame();
	}
	
	public void conveyMessage(String message) {
		textArea.append(message);
	}
	
	private void startGame(){
		gameStopped = false;
		whitePlayerThread = new Thread(whitePlayer);
		blackPlayerThread = new Thread(blackPlayer);
		whitePlayerThread.start(); blackPlayerThread.start();
	}
	
	private void stopGame(){
		gameStopped = true;
	}
	
	public int inquirePromotionTypeFromCurrentMover(boolean wtm){
		return wtm?whitePlayer.inquirePromotionType():blackPlayer.inquirePromotionType();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == newGame) {
			/*stopGame();
			gameStates.clear();
			pgn.clear(); cn.clear();
			board.setGameState(GameState.initialPosition);
			gameStates.add(GameState.initialPosition);
			board.movSquare1 = 65; board.movSquare2 = 65;
			textArea.setText("");
			startGame();*/
			Chess chess = new Chess();
			this.dispose();
		} else if (e.getSource() == exportPGN){
			JFileChooser save = new JFileChooser();
			save.setFileFilter(new FileNameExtensionFilter("PGN file", ".pgn"));
			int option = save.showSaveDialog(this);
			if (option == JFileChooser.APPROVE_OPTION) {
				try {
					// create a buffered writer to write to a file
					BufferedWriter out = new BufferedWriter(new FileWriter(save.getSelectedFile().getPath()));
					String text = "";
					for (String ply : pgn) text += ply;
					out.write(text); // write the contents of the TextArea to the file
					out.close(); // close the file stream
				} catch (Exception ex) {
					System.out.println(ex.getMessage());
				}
			}
		} else if (e.getSource() == openBook) {
			JFileChooser open = new JFileChooser(); // open up a file chooser (a dialog for the user to browse files to open)
			int option = open.showOpenDialog(this); // get the option that the user selected (approve or cancel)
			if (option == JFileChooser.APPROVE_OPTION) {
				conveyMessage("Book opened.");
			}
		} else if (e.getSource() == quit) {
			this.dispose(); // Dispose all resources and close the application.
		} else if (e.getSource() == flipBoard) {
			board.flipBoard();
		} else if (e.getSource() == whitePlayerHuman) {
			if (!whiteIsHuman) whitePlayer = new HumanPlayer();
			whiteIsHuman = true;
			startGame();
		} else if (e.getSource() == blackPlayerHuman) {
			if (!blackIsHuman) 	blackPlayer = new HumanPlayer();
			blackIsHuman = true;
			startGame();
		} else if (e.getSource() == whitePlayerAI) {
			if (whiteIsHuman) whitePlayer = new AIPlayer(this, true);
			whiteIsHuman = false;
			startGame();
		} else if (e.getSource() == blackPlayerAI) {
			if (blackIsHuman) blackPlayer = new AIPlayer(this, false);
			blackIsHuman = false;
			startGame();
		}
	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		if (e.getSource() == autoSwitchPOV){
			board.autoSwitchPOV = e.getStateChange() == ItemEvent.SELECTED;
		}
	}
}
