import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;


public class Board extends JPanel implements MouseListener{

	public static int DEFAULT_WIDTH = 640;
	public static int DEFAULT_HEIGHT = 640;
	public static int PIECE_PADDING_CONSTANT = 12;
	
	public static BufferedImage lightSquare;
	public static BufferedImage darkSquare;
	
	public static BufferedImage wpawn;
	public static BufferedImage wrook;
	public static BufferedImage wknight;
	public static BufferedImage wbishop;
	public static BufferedImage wqueen;
	public static BufferedImage wking;
	
	public static BufferedImage bpawn;
	public static BufferedImage brook;
	public static BufferedImage bknight;
	public static BufferedImage bbishop;
	public static BufferedImage bqueen;
	public static BufferedImage bking;
	
	private Chess chessGame;
	private GameState gameState;
	private int highlightedSquare;
	public int movSquare1 = -1;
	public int movSquare2 = -1;
	private int squareSize;
	public boolean autoSwitchPOV;
	private boolean whitePOV;
	
	public Board(GameState initialState, Chess chessGame){
		Class clazz = this.getClass();
		try {
			Board.lightSquare = ImageIO.read(clazz.getResource("light.png"));
			Board.darkSquare  = ImageIO.read(clazz.getResource("dark.png"));
			
			Board.wpawn		= ImageIO.read(clazz.getResource("wpawn.png"));
			Board.wrook		= ImageIO.read(clazz.getResource("wrook.png"));
			Board.wknight	= ImageIO.read(clazz.getResource("wknight.png"));
			Board.wbishop	= ImageIO.read(clazz.getResource("wbishop.png"));
			Board.wqueen	= ImageIO.read(clazz.getResource("wqueen.png"));
			Board.wking		= ImageIO.read(clazz.getResource("wking.png"));
			
			Board.bpawn		= ImageIO.read(clazz.getResource("bpawn.png"));
			Board.brook		= ImageIO.read(clazz.getResource("brook.png"));
			Board.bknight	= ImageIO.read(clazz.getResource("bknight.png"));
			Board.bbishop	= ImageIO.read(clazz.getResource("bbishop.png"));
			Board.bqueen	= ImageIO.read(clazz.getResource("bqueen.png"));
			Board.bking		= ImageIO.read(clazz.getResource("bking.png"));
		} catch (IOException e) {
			System.err.println("Could not find image file for one or more pieces.");
			e.printStackTrace();
		}
		
		setPreferredSize(new Dimension(DEFAULT_WIDTH, DEFAULT_HEIGHT));
		this.chessGame = chessGame;
		gameState = initialState; whitePOV = true; autoSwitchPOV = false;
		highlightedSquare = 65; squareSize = 1;
		addMouseListener(this);
	}
	
	public void setGameState(GameState gs){
		gameState = gs;
		repaint();
	}
	
	public void flipBoard() {
		whitePOV = !whitePOV;
		repaint();
	}
	
	public void flipIfAuto() {
		if (autoSwitchPOV && (chessGame.currentState().whiteToMove() != whitePOV)
				&& (chessGame.currentState().whiteToMove()?chessGame.whiteIsHuman:chessGame.blackIsHuman))
			flipBoard();
	}
	
	@Override
    public void paintComponent(Graphics g) {
         super.paintComponent(g);
         int x = 0; int y = 0;
         squareSize = Math.min(getWidth()/8, getHeight()/8);
         ////DRAW SQUARES AND PIECES////
         BufferedImage img = darkSquare;
         BufferedImage pieceImg = null;
         for (byte squareNum = 0; squareNum < 64; ++squareNum){
        	if (whitePOV) { x = (squareNum%8)*squareSize; y = (7 - squareNum/8)*squareSize;}
        	else		  { x = (7 - squareNum%8)*squareSize; y = (squareNum/8)*squareSize;}
        	g.drawImage(img, x, y, x+squareSize, y+squareSize, 0, 0, img.getWidth(), img.getHeight(), null);
        	pieceImg = pieceAt(squareNum);
        	if (pieceImg != null){
        		int pad = squareSize/PIECE_PADDING_CONSTANT;
        		g.drawImage(pieceImg, x+pad, y+pad, x+squareSize-pad, y+squareSize-pad, 0, 0, pieceImg.getWidth(), pieceImg.getHeight(), null);
        	}
	    	if (squareNum%8 < 7) //If not at the end of a row, switch to other square color.
	    		img = img.equals(darkSquare) ? lightSquare : darkSquare;
         }
         ////DRAW UI EFFECTS////
         if (0 <= highlightedSquare && highlightedSquare < 64){
        	 g.setColor(Color.blue);
        	 if (whitePOV) { x = (highlightedSquare%8)*squareSize; y = (7 - highlightedSquare/8)*squareSize;}
         	else		  { x = (7 - highlightedSquare%8)*squareSize; y = (highlightedSquare/8)*squareSize;}
        	 g.drawOval(x, y, squareSize, squareSize); 
         }
         	g.setColor(Color.red);
         if (0 <= movSquare1 && movSquare1 < 64 && 0 <= movSquare2 && movSquare2 < 64) {
        	 if (whitePOV) { x = (movSquare1%8)*squareSize; y = (7 - movSquare1/8)*squareSize;}
          	else		  { x = (7 - movSquare1%8)*squareSize; y = (movSquare1/8)*squareSize;}
         	 g.drawOval(x, y, squareSize, squareSize);
         	if (whitePOV) { x = (movSquare2%8)*squareSize; y = (7 - movSquare2/8)*squareSize;}
          	else		  { x = (7 - movSquare2%8)*squareSize; y = (movSquare2/8)*squareSize;}
         	 g.drawOval(x, y, squareSize, squareSize);
         }
     }
	 
	 private BufferedImage pieceAt(int squareNum){
		 switch (gameState.pieceType(squareNum)) {
		 	case GameState.BLANK0:
		 		return null;
		 	case GameState.BLANK1:
		 		return null;
		 	case GameState.PAWN:
		 		return gameState.isWhite(squareNum) ? wpawn : bpawn;
		 	case GameState.ROOK:
		 		return gameState.isWhite(squareNum) ? wrook : brook;
		 	case GameState.KNIGHT:
		 		return gameState.isWhite(squareNum) ? wknight : bknight;
		 	case GameState.BISHOP:
		 		return gameState.isWhite(squareNum) ? wbishop : bbishop;
		 	case GameState.QUEEN:
		 		return gameState.isWhite(squareNum) ? wqueen : bqueen;
		 	case GameState.KING:
		 		return gameState.isWhite(squareNum) ? wking : bking;
		 }
		 return null;
	 }

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void mousePressed(MouseEvent e) {
		squareSize = Math.min(getWidth()/8, getHeight()/8);
		highlightedSquare = e.getX()/squareSize + 56 - ((e.getY()/squareSize)<<3);
	 	 if (!whitePOV) highlightedSquare = 63 - highlightedSquare;
		repaint();
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		if (0 <= highlightedSquare && highlightedSquare < 64){
			int hoveredSquare = e.getX()/squareSize + 56 - ((e.getY()/squareSize)<<3);
		 	 if (!whitePOV) hoveredSquare = 63 - hoveredSquare;
			chessGame.doIfLegal(highlightedSquare, hoveredSquare);
		}
		highlightedSquare = 65;
		repaint();
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
}
