import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.Arrays;

import javax.swing.JPanel;


public class InfoPanel extends JPanel{
	
	private Chess game;
	private int[] capturedWhites;
	private byte nextEmptyW = 0;
	private int[] capturedBlacks;
	private byte nextEmptyB = 0;
	
	public static final byte PAWN = 0;
	public static final byte KNIGHT = 1;
	public static final byte BISHOP = 2;
	public static final byte ROOK = 3;
	public static final byte QUEEN = 4;
	
	public InfoPanel(Chess game) {
		this.game = game;
		int a = Integer.MAX_VALUE;
		capturedWhites = new int[]{a,a,a,a, a,a,a,a, a,a,a,a, a,a,a,a};
		capturedBlacks = new int[]{a,a,a,a, a,a,a,a, a,a,a,a, a,a,a,a};
	}
	
	public void addCapturedPiece(int pieceType, boolean isWhite) {
		switch (pieceType) {
		case GameState.PAWN:
			appendToArray(PAWN, isWhite);
			break;
		case GameState.KNIGHT:
			appendToArray(KNIGHT, isWhite);
			break;
		case GameState.BISHOP:
			appendToArray(BISHOP, isWhite);
			break;
		case GameState.ROOK:
			appendToArray(ROOK, isWhite);
			break;
		case GameState.QUEEN:
			appendToArray(QUEEN, isWhite);
			break;
		}
		Arrays.sort(capturedWhites);
		Arrays.sort(capturedBlacks);
		repaint();
	}
	
	private void appendToArray(int val, boolean whiteAr) {
		if (whiteAr) {
			capturedWhites[nextEmptyW] = val;
			++nextEmptyW;
		} else {
			capturedBlacks[nextEmptyB] = val;
			++nextEmptyB;
		}
	}
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		int size = Math.min(getWidth()/4, getHeight()/8); int x; int y;
		BufferedImage img; boolean onW = true;
		do {
			int place = 0;
			for (byte i = 0; i < (onW?nextEmptyW:nextEmptyB); ++i) {
				x = (place%4)*size;
				y = (onW?0:4*getHeight()/8) + (place/4)*size;
				switch (onW?capturedWhites[i]:capturedBlacks[i]) {
				case PAWN:
					img = onW?Board.wpawn:Board.bpawn;
					g.drawImage(img, x, y, x+size, y+size, 0, 0, img.getWidth(), img.getHeight(), null);
					break;
				case KNIGHT:
					img = onW?Board.wknight:Board.bknight;
					g.drawImage(img, x, y, x+size, y+size, 0, 0, img.getWidth(), img.getHeight(), null);
					break;
				case BISHOP:
					img = onW?Board.wbishop:Board.bbishop;
					g.drawImage(img, x, y, x+size, y+size, 0, 0, img.getWidth(), img.getHeight(), null);
					break;
				case ROOK:
					img = onW?Board.wrook:Board.brook;
					g.drawImage(img, x, y, x+size, y+size, 0, 0, img.getWidth(), img.getHeight(), null);
					break;
				case QUEEN:
					img = onW?Board.wqueen:Board.bqueen;
					g.drawImage(img, x, y, x+size, y+size, 0, 0, img.getWidth(), img.getHeight(), null);
					break;
				}
				++place;
			}
			onW = !onW;
		} while (!onW);
	}

}
