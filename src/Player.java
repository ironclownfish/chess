
public interface Player extends Runnable {

	public void makeMove(GameState currState);
	public int inquirePromotionType();
}
