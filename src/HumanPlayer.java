import javax.swing.JOptionPane;


public class HumanPlayer implements Player{

	public void makeMove(GameState currState){
		//Nothing. Human will make it magically happen.
	}

	@Override
	public void run() {
		//Nothing. Human will make it magically happen.
	}

	@Override
	public int inquirePromotionType() {

		switch(JOptionPane.showOptionDialog(null, "", "Promotion", JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null,
				new String[]{"Knight", "Bishop", "Rook", "Queen"}, 0)){
		case 0:
			return GameState.KNIGHT;
		case 1:
			return GameState.BISHOP;
		case 2:
			return GameState.ROOK;
		}
		return GameState.QUEEN;
		/*promotionType = 99;
		ImageIcon knightIcon = new ImageIcon((Image)(wtm?Board.wknight:Board.bknight));
		JButton knightButton = new JButton(); knightButton.setIcon(knightIcon);
		ImageIcon bishopIcon = new ImageIcon((Image)(wtm?Board.wbishop:Board.bbishop));
		JButton bishopButton = new JButton(); bishopButton.setIcon(bishopIcon);
		ImageIcon rookIcon = new ImageIcon((Image)(wtm?Board.wrook:Board.brook));
		JButton rookButton = new JButton(); rookButton.setIcon(rookIcon);
		ImageIcon queenIcon = new ImageIcon((Image)(wtm?Board.wqueen:Board.bqueen));
		JButton queenButton = new JButton(); queenButton.setIcon(queenIcon);
		
		knightButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				Chess.promotionType = GameState.KNIGHT;
			}
		});
		bishopButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				Chess.promotionType = GameState.BISHOP;
			}
		});
		rookButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				Chess.promotionType = GameState.ROOK;
			}
		});
		queenButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				Chess.promotionType = GameState.QUEEN;
			}
		});
		
		JFrame menu = new JFrame("Promotion");
		menu.setLayout(new GridLayout()); menu.setResizable(false);
		menu.add(knightButton); menu.add(bishopButton); menu.add(rookButton); menu.add(queenButton);
		menu.pack();
		menu.setLocationRelativeTo(null);
		menu.setVisible(true);*/
	}
}
