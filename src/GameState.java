/*
 * Hidden info
 * in the basis of blank colors.
 * 1 : 1 : 12 : 1 : 1 : 2 : 3 : 4 : 7
 * to move : mover in check : King positions : 0 : valid en passant : repetition : en passant : castling rights : 50 move rule
 * ------King positions--------
 * 3 : 3 : 3 : 3
 * white col : white row : black col : black row
 * ------caslting rights:------
 * 1 : 1 : 1 : 1
 * white has Q : white has K : black has Q : black has K
 * ----------------------------
 * -------en passant-----------
 * 3
 * The column number where an
 * apparent en passant is actually
 * valid.
 * ----------------------------
 * 
 * 
 * @author Nicholas Gyd�
 */

public class GameState {
	
	public static GameState initialPosition = 
			  new GameState(0xFFFF209c3f800000l, 0xEF000000000000EFl, 0x99FF00000000FF99l, 0x3C0000000000003Cl, 0x209c3f80);

	public static final byte PAWN = 0;
	public static final byte ROOK = 1;
	public static final byte KNIGHT = 2;
	public static final byte BISHOP = 3;
	public static final byte QUEEN = 4;
	public static final byte KING = 5;
	public static final byte BLANK0 = 6;
	public static final byte BLANK1 = 7;
	
	public static final int NO_PIN = 99;
	
	//bit boards
	public long color;
	public long xxPK_KnBRQ;
	public long leafTypeGen;
	public long leafTypeSpc;
	public int info;
	
	public GameState(long color, long xxPK_KnBRQ, long leafTypeGen, long leafTypeSpc, int info){
		this.color = color;
		this.xxPK_KnBRQ = xxPK_KnBRQ;
		this.leafTypeGen = leafTypeGen;
		this.leafTypeSpc = leafTypeSpc;
		hideInfo(info);
	}
	
	public boolean whiteToMove(){
		return (hiddenInfo() & 0x80000000) == 0;
	}
	public boolean blackToMove(){
		return !whiteToMove();
	}
	
	/*
	 * Returns true iff moving the piece on square number
	 * sq1 to square number sq2 constitutes a legal ply in
	 * this game state.
	 */
	public boolean legalMove(int sq1, int sq2){
		if (0 > sq1 || sq1 > 63 || 0 > sq2 || sq2 > 63) return false;
		int temp = 0;
		//Make sure not null move.
		if (sq1 == sq2) return false;
		int movingType = pieceType(sq1);
		//Make sure that sq1 has a piece of the correct color.
		if (movingType == BLANK0 || movingType == BLANK1) return false;
		if (isWhite(sq1) != whiteToMove())				  return false;
		/*//Make sure that this move doesn't cause a discovered check.
		temp = checkPin(sq1, sq2); //We need sq2 because pins depend on en passants
		if (temp != sq1 && checkPin(sq2, sq2) == sq2 && sq2 != temp) return false;
		//Make sure that, if we're not moving the king while he's in check, we're blocking the check. //checkPin(sq2,sq2) == sq2
		if (movingType != KING && moverInCheck() && afterMove(sq1, sq2, true)[0].inDangerFrom(kingCol(), kingRow(), !whiteToMove())) return false; //TODO
		//Make sure that, if we're moving the king, he isn't moving into check.
		if (movingType == KING && afterMove(sq1, sq2, true)[0].inDangerFrom(sq2%8, sq2/8, !whiteToMove())) return false;*/
		GameState afterMove = afterMove(sq1, sq2, true, null)[0];
		if(afterMove.inDangerFrom(afterMove.kingCol(whiteToMove()), afterMove.kingRow(whiteToMove()), !whiteToMove())) return false;
		
		//Make sure that sq2 is within the piece's range of movement given
		//current circumstances.
		switch (movingType) {
		case PAWN:
			int capturedType = pieceType(sq2);
			if (capturedType == BLANK0 || capturedType == BLANK1){ //Non-capturing case.
				////Check for en passant
				if (whiteToMove()) temp = sq2/8 - sq1/8;
				else	temp = sq1/8 - sq2/8;
				if (sq1/8 == (whiteToMove()?4:3) && Math.abs(sq1%8 - sq2%8) == 1
				 && temp == 1 && pieceType(sq2+(whiteToMove()?-8:8)) == PAWN
				 && isWhite(sq2+(whiteToMove()?-8:8)) != whiteToMove()) {
					//Check whether this apparent en passant is validated by hidden info.
					temp = (hiddenInfo()>>>11)%8;
					if(sq2%8 == temp && (hiddenInfo()>>>16)%2 == 1) return true;
				}
				////
				//Make sure sq2 is in pawn's column.
				if (sq2%8 != sq1%8) return false;
				//Make sure sq2 is not too far ahead or behind the pawn.
				if (whiteToMove()) temp = sq2/8 - sq1/8;
				else	temp = sq1/8 - sq2/8;
				if (temp < 1 || temp > 2 || (temp==2 && sq1/8 != (whiteToMove()?1:6))) return false;
				//Make sure the pawn's path isn't blocked.
				if (whiteToMove()) temp = pieceType(sq1+8);
				else temp = pieceType(sq1-8);
				if (temp != BLANK0 && temp != BLANK1) return false;
			} else { //Capturing case
				//Check that captured piece is opposite color
				if ( isWhite(sq2) == whiteToMove() )return false;
				//Check that captured piece is diagonal 1 square left or right pawn.
				if (Math.abs(sq1%8 - sq2%8) != 1) return false;
				//Check that captured piece is 1 square in front of pawn.
				if (whiteToMove()) temp = sq2/8 - sq1/8;
				else	temp = sq1/8 - sq2/8;
				if(temp != 1) return false;
			}
			break;
		case ROOK:
			//Make sure the move is along a straight line.
			temp = sq2/8 - sq1/8;
			if (temp != 0 && sq2%8 != sq1%8 ) return false;
			//Make sure there aren't any obstructions.
			temp = temp==0 ? Integer.signum(sq2-sq1) : Integer.signum(sq2-sq1)<<3; //moving along row or col
			for (int sqr = sq1 + temp; sqr != sq2; sqr += temp){
				if(pieceType(sqr) < BLANK0) return false;
			}
			if (pieceType(sq2) < BLANK0 && isWhite(sq2) == isWhite(sq1)) return false;
			break;
		case KNIGHT:
			//Make sure the move is a knight's move away.
			temp = sq2/8 - sq1/8;
			if( Math.abs(temp*(sq2%8 - sq1%8)) != 2 || Math.abs(temp) + Math.abs(sq2%8 - sq1%8) != 3) return false;
			if(pieceType(sq2) < BLANK0 && isWhite(sq2) == isWhite(sq1)) return false;
			break;
		case BISHOP:
			//Make sure destination is diagonal from bishop.
			temp = sq2/8 - sq1/8;
			if (Math.abs(temp) != Math.abs(sq2%8 - sq1%8)) return false;
			//Make sure there are no obstructions.
			if(pieceType(sq2) < BLANK0 && isWhite(sq2) == isWhite(sq1)) return false;
			temp = Integer.signum(temp)<<3;
			int temp2 = Integer.signum(sq2%8 - sq1%8);
			for (int square = sq1+temp+temp2; square != sq2; square += temp+temp2 ){
				if (pieceType(square) < BLANK0) return false;
			}
			break;
		case QUEEN:
			temp = sq2/8 - sq1/8;
			if ((temp != 0 && sq2%8 != sq1%8)
			&& Math.abs(temp) != Math.abs(sq2%8 - sq1%8)) return false;
			//Make sure there are no obstructions.
			if(pieceType(sq2) < BLANK0 && isWhite(sq2) == isWhite(sq1)) return false;
			temp = Integer.signum(temp)<<3;
			int temp3 = Integer.signum(sq2%8 - sq1%8);
			for (int square = sq1+temp+temp3; square != sq2; square += temp+temp3 ){
				if (pieceType(square) < BLANK0) return false;
			}
			break;
		case KING:
			if (pieceType(sq2) < BLANK0 && isWhite(sq2) == whiteToMove()) return false;
			temp = sq2%8 - sq1%8;
			//----castling
			if (temp == 2 && whiteToMove() && !moverInCheck() && sq1 == 4 && whiteHasCastleKing() && !inDangerFrom(5, 0, false) && !inDangerFrom(6, 0, false) && pieceType(5) >= BLANK0) return true;
			if (temp == 2 && !whiteToMove()&& !moverInCheck() && sq1 == 60&& blackHasCastleKing() && !inDangerFrom(5, 7,  true) && !inDangerFrom(6, 7,  true) && pieceType(61) >= BLANK0) return true;
			if (temp ==-2 && whiteToMove() && !moverInCheck() && sq1 == 4 && whiteHasCastleQueen()&& !inDangerFrom(3, 0, false) && !inDangerFrom(2, 0, false) && pieceType(3) >= BLANK0 && pieceType(2) >= BLANK0 && pieceType(1) >= BLANK0) return true;
			if (temp ==-2 && !whiteToMove()&& !moverInCheck() && sq1 == 60&& blackHasCastleQueen()&& !inDangerFrom(3, 7,  true) && !inDangerFrom(2, 7,  true) && pieceType(59)>= BLANK0 && pieceType(58)>= BLANK0 && pieceType(57)>= BLANK0) return true;
			//----
			if (Math.abs(sq2/8 - sq1/8) > 1 || Math.abs(temp) > 1) return false;
		}
		return true;
	}
	
	/*
	 * 
	 */
	private int checkPin(int sq1, int sq2) {
		//Get ray from king to sq1.
		int dx = sq1%8 - kingCol();
		int dy = sq1/8 - kingRow();
		if (dx != 0 && dy != 0 && Math.abs(dx)!=Math.abs(dy)) return sq1;
		//Manhattan normalize ray.
		dx = Integer.signum(dx);
		dy = Integer.signum(dy);
		for (int square = kingSqr() + (dy<<3) + dx; square < 74 && square > -10; square += (dy<<3) + dx){
			if (pieceType(square) < BLANK0 && isWhite(square) == whiteToMove() && square!=sq1) return sq1;
			if (	isWhite(square) != whiteToMove()){
				if (pieceType(square) == ((dx==0||dy==0)?ROOK:BISHOP)
				 || pieceType(square) == QUEEN) return square;
				else {
					//CHECK THAT THIS IS NOT A PAWN DOOMED BY AN EN PASSANT
					if (pieceType(square) < BLANK0 && !(sq1!=sq2 && pieceType(square) == PAWN && sq2==square+(whiteToMove()?8:-8)))
						return sq1;
					/////////					
				}
			}
				
			if (square%8 < 0 || square%8 > 7 || square/8 < 0 || square/8 > 7) return sq1;
			if (dx == 0 && dy == 0) break;
		}
		return sq1;
	}

	/*
	 * Returns a new game state in which sq1 has been moved to
	 * sq2, the 50 move rule has been updated if necessary,
	 * the color is set to the opposite of the moved piece,
	 * and castling and en passant rights have been updated.
	 */
	public GameState[] afterMove(int sq1, int sq2, boolean hypothetical, Chess game){
		long newColor = color;
		long newxxPK_KnBRQ = xxPK_KnBRQ;
		long newleafTypeGen = leafTypeGen;
		long newleafTypeSpc = leafTypeSpc;
		int newInfo = hiddenInfo();
		boolean multiplePromotionScenarios = false;
		
		newColor = setBit(newColor, sq2, (color>>>sq1)%2);
		
		newxxPK_KnBRQ = setBit(newxxPK_KnBRQ, sq2, (xxPK_KnBRQ>>>sq1)%2); //set sq2 to sq1
		newxxPK_KnBRQ = setBit(newxxPK_KnBRQ, sq1, 0);  //clear square 1
		
		newleafTypeGen = setBit(newleafTypeGen, sq2, (leafTypeGen>>>sq1)%2); //set sq2 to sq1
		newleafTypeGen = setBit(newleafTypeGen, sq1, 0);  //clear square 1
		
		newleafTypeSpc = setBit(newleafTypeSpc, sq2, (leafTypeSpc>>>sq1)%2); //set sq2 to sq1
		newleafTypeSpc = setBit(newleafTypeSpc, sq1, 0);  //clear square 1
		
		//If move was en passant, remove captured pawn
		boolean whiteToMove = whiteToMove();
		int temp = whiteToMove ? sq2/8 - sq1/8 : sq1/8 - sq2/8;
		if ((hiddenInfo()>>>16)%2==1 && pieceType(sq1) == PAWN && sq1/8 == (whiteToMove?4:3) && Math.abs(sq1%8 - sq2%8) == 1
		 && temp == 1 && pieceType(sq2+(whiteToMove?-8:8)) == PAWN
		 && isWhite(sq2+(whiteToMove?-8:8)) != whiteToMove) {
			newleafTypeGen = setBit(newleafTypeGen, sq2+(whiteToMove?-8:8), 0);  //clear pawn
		}
		
		//If move was promotion, and this is not hypothetical, change pawn to promoted piece.
		if (pieceType(sq1) == PAWN && sq2/8 == (whiteToMove?7:0)) {
			if (!hypothetical) {
				temp = game.inquirePromotionTypeFromCurrentMover(whiteToMove);
				//newColor = setBit(newColor, sq2, (sq2==0)?1:0); // TODO I think this is wrong.
				newxxPK_KnBRQ = setBit(newxxPK_KnBRQ, sq2, 1);
				if (temp == KNIGHT) {newleafTypeGen = setBit(newleafTypeGen, sq2, 0); newleafTypeSpc = setBit(newleafTypeSpc, sq2, 0);}
				if (temp == BISHOP) {newleafTypeGen = setBit(newleafTypeGen, sq2, 0); newleafTypeSpc = setBit(newleafTypeSpc, sq2, 1);}
				if (temp == ROOK)	{newleafTypeGen = setBit(newleafTypeGen, sq2, 1); newleafTypeSpc = setBit(newleafTypeSpc, sq2, 0);}
				if (temp == QUEEN)	{newleafTypeGen = setBit(newleafTypeGen, sq2, 1); newleafTypeSpc = setBit(newleafTypeSpc, sq2, 1);}
			} //Case where hypothetical is dealt with later.
		}
		
		/////UPDATE INFO/////
		newInfo ^= 0x80000000; //flip 'toMove' bit
		// update 50 move rule
			if 		((pieceType(sq2) < BLANK0)
			 	||	 (pieceType(sq1)  == PAWN)) newInfo &= 0xffffff80;
			else if (newInfo%7 < 120) ++newInfo;
		// update en passant
			newInfo &= 0xffffc7ff;
			if (pieceType(sq1)  == PAWN //A pawn moved...
			&& sq2/8 - sq1/8	== (whiteToMove?2:-2) //...two squares forward...
			&& ((pieceType(sq2-1)== PAWN && isWhite(sq2-1) != whiteToMove)
			   ||pieceType(sq2+1)== PAWN && isWhite(sq2+1) != whiteToMove)) {
				//...ending up left or right of an opposite colored pawn.
				newInfo |= (sq2%8)<<11;
				newInfo |= 0x10000; //DEBUG
			} else {
				/*for (int square = (whiteToMove?24:32); square < (whiteToMove?32:40); ++square){
					if ((isWhite(square) != whiteToMove && pieceType(square) < BLANK0) || square%8 == 7) {
						newInfo |= (square%8)<<11;
						break;
					}
				}*/
				newInfo &= 0xfffeffff; //DEBUG
			}
		// update king position if necessary.
			if (pieceType(sq1) == KING) {
				newInfo &= whiteToMove?0xc0ffffff:0xff03ffff;
				newInfo += (((sq2%8)<<(whiteToMove?27:21)) + ((sq2/8)<<(whiteToMove?24:18)));
			//Update castling rights.
				if (whiteToMove) newInfo &= 0xfffff9ff; else newInfo &= 0xfffffe7f;
			// If castled, update rook position.
				temp = sq2%8 - sq1%8;
				int r1=7; int r2=5;
				if ((temp ==-2 && whiteToMove)) {r1=0; r2=3;}
				if ((temp == 2 &&!whiteToMove)) {r1=63;r2=61;}
				if ((temp ==-2 &&!whiteToMove)) {r1=56;r2=59;}
				
				if (Math.abs(temp) == 2) {
				newColor = setBit(newColor, r2, (color>>>sq1)%2);
				
				newxxPK_KnBRQ = setBit(newxxPK_KnBRQ, r2, (xxPK_KnBRQ>>>r1)%2); //set r2 to r1
				newxxPK_KnBRQ = setBit(newxxPK_KnBRQ, r1, 0);  //clear r1
				
				newleafTypeGen = setBit(newleafTypeGen, r2, (leafTypeGen>>>r1)%2); //set r2 to r1
				newleafTypeGen = setBit(newleafTypeGen, r1, 0);  //clear r1
				
				newleafTypeSpc = setBit(newleafTypeSpc, r2, (leafTypeSpc>>>r1)%2); //set r2 to r1
				newleafTypeSpc = setBit(newleafTypeSpc, r1, 0);  //clear r1
				}
			}
			if (pieceType(sq1) == ROOK){
				// update castling rights.
				if (sq1 == 0) newInfo &= 0xfffffbff;
				if (sq1 == 56)newInfo &= 0xfffffeff;
				if (sq1 == 7) newInfo &= 0xfffffdff;
				if (sq1 == 63)newInfo &= 0xffffff7f;
			}
			
		GameState result = new GameState(newColor, newxxPK_KnBRQ, newleafTypeGen, newleafTypeSpc, newInfo);
		//result.hideInfo(newInfo);
		GameState[] resultAr = new GameState[]{result};
		
		if (multiplePromotionScenarios) { //Deal with promotion scenarios if in hypothetical mode.
				GameState choseKnight = new GameState(newColor, newxxPK_KnBRQ, newleafTypeGen, newleafTypeSpc, newInfo); //newInfo shouldn't be passed in here?
				GameState choseBishop = new GameState(newColor, newxxPK_KnBRQ, newleafTypeGen, newleafTypeSpc, newInfo);
				GameState choseRook	  = new GameState(newColor, newxxPK_KnBRQ, newleafTypeGen, newleafTypeSpc, newInfo);
				GameState choseQueen  = new GameState(newColor, newxxPK_KnBRQ, newleafTypeGen, newleafTypeSpc, newInfo);
				
				choseKnight.xxPK_KnBRQ = setBit(newxxPK_KnBRQ, sq2, 1);
				choseBishop.xxPK_KnBRQ = setBit(newxxPK_KnBRQ, sq2, 1);
				choseRook.xxPK_KnBRQ = setBit(newxxPK_KnBRQ, sq2, 1);
				choseQueen.xxPK_KnBRQ = setBit(newxxPK_KnBRQ, sq2, 1);
				
				
				choseKnight.leafTypeGen = setBit(newleafTypeGen, sq2, 0); choseKnight.leafTypeSpc = setBit(newleafTypeSpc, sq2, 0);
				choseBishop.leafTypeGen = setBit(newleafTypeGen, sq2, 0); choseBishop.leafTypeSpc = setBit(newleafTypeSpc, sq2, 1);
				choseRook.leafTypeGen = setBit(newleafTypeGen, sq2, 1); choseRook.leafTypeSpc = setBit(newleafTypeSpc, sq2, 0);
				choseQueen.leafTypeGen = setBit(newleafTypeGen, sq2, 1); choseQueen.leafTypeSpc = setBit(newleafTypeSpc, sq2, 1);
				
				resultAr = new GameState[]{choseQueen, choseKnight, choseBishop, choseRook};
		}
		
		for (GameState res : resultAr) {
		// update check
			int myNewInfo = newInfo & 0xbfffffff;
			if (res.inDangerFrom(res.kingCol(!whiteToMove), res.kingRow(!whiteToMove), whiteToMove)) {
				myNewInfo |= 0x40000000;
			}
			res.hideInfo(myNewInfo);
		}
		
		return resultAr;
	}
	
	private boolean inDangerFrom(int col, int row, boolean whiteIsWhom){ //Wraparound problems PROBABLY fixed.
		return  (col<7 && col+1 + (row<<3)+(whiteIsWhom?-8:8) > 0 && col+1 + (row<<3)+(whiteIsWhom?-8:8) < 63 && isWhite(col+1 + (row<<3)+(whiteIsWhom?-8:8)) == whiteIsWhom && pieceType(col+1 + (row<<3)+(whiteIsWhom?-8:8)) == PAWN)
			||	(col>0 && col-1 + (row<<3)+(whiteIsWhom?-8:8) > 0 && col-1 + (row<<3)+(whiteIsWhom?-8:8) < 63 && isWhite(col-1 + (row<<3)+(whiteIsWhom?-8:8)) == whiteIsWhom && pieceType(col-1 + (row<<3)+(whiteIsWhom?-8:8)) == PAWN)
			||  (col<7 && col+1 + (row<<3)+ 8 > 0 && col+1 + (row<<3)+ 8 < 63 && isWhite(col+1 + (row<<3)+ 8) == whiteIsWhom && pieceType(col+1 + (row<<3)+ 8) == KING)
			||	(col>0 && col-1 + (row<<3)+ 8 > 0 && col-1 + (row<<3)+ 8 < 63 && isWhite(col-1 + (row<<3)+ 8) == whiteIsWhom && pieceType(col-1 + (row<<3)+ 8) == KING)
			||  (col   + (row<<3)+ 8 > 0 && col   + (row<<3)+ 8 < 63 && isWhite(col   + (row<<3)+ 8) == whiteIsWhom && pieceType(col   + (row<<3)+ 8) == KING)
			||  (col<7 && col+1 + (row<<3)- 8 > 0 && col+1 + (row<<3)- 8 < 63 && isWhite(col+1 + (row<<3)- 8) == whiteIsWhom && pieceType(col+1 + (row<<3)- 8) == KING)
			||	(col>0 && col-1 + (row<<3)- 8 > 0 && col-1 + (row<<3)- 8 < 63 && isWhite(col-1 + (row<<3)- 8) == whiteIsWhom && pieceType(col-1 + (row<<3)- 8) == KING)
			||  (col   + (row<<3)- 8 > 0 && col   + (row<<3)- 8 < 63 && isWhite(col   + (row<<3)- 8) == whiteIsWhom && pieceType(col   + (row<<3)- 8) == KING)
			||  (col<7 && col+1 + (row<<3)    > 0 && col+1 + (row<<3)    < 63 && isWhite(col+1 + (row<<3)   ) == whiteIsWhom && pieceType(col+1 + (row<<3)   ) == KING)
			||	(col>0 && col-1 + (row<<3)    > 0 && col-1 + (row<<3)    < 63 && isWhite(col-1 + (row<<3)   ) == whiteIsWhom && pieceType(col-1 + (row<<3)   ) == KING)
			||	(col<6 && col+2 + 8 +(row<<3) > 0 && col+2 + 8 +(row<<3) < 63 && pieceType(col+2 + 8 +(row<<3)) == KNIGHT && isWhite(col+2 + 8 +(row<<3)) == whiteIsWhom)
			||  (col>1 && col-2 + 8 +(row<<3) > 0 && col-2 + 8 +(row<<3) < 63 && pieceType(col-2 + 8 +(row<<3)) == KNIGHT && isWhite(col-2 + 8 +(row<<3)) == whiteIsWhom)
			||  (col<6 && col+2 - 8 +(row<<3) > 0 && col+2 - 8 +(row<<3) < 63 && pieceType(col+2 - 8 +(row<<3)) == KNIGHT && isWhite(col+2 - 8 +(row<<3)) == whiteIsWhom)
			||  (col>1 && col-2 - 8 +(row<<3) > 0 && col-2 - 8 +(row<<3) < 63 && pieceType(col-2 - 8 +(row<<3)) == KNIGHT && isWhite(col-2 - 8 +(row<<3)) == whiteIsWhom)
			||  (col<7 && col+1 +16 +(row<<3) > 0 && col+1 +16 +(row<<3) < 63 && pieceType(col+1 +16 +(row<<3)) == KNIGHT && isWhite(col+1 +16 +(row<<3)) == whiteIsWhom)
			||  (col>0 && col-1 +16 +(row<<3) > 0 && col-1 +16 +(row<<3) < 63 && pieceType(col-1 +16 +(row<<3)) == KNIGHT && isWhite(col-1 +16 +(row<<3)) == whiteIsWhom)
			||  (col<7 && col+1 -16 +(row<<3) > 0 && col+1 -16 +(row<<3) < 63 && pieceType(col+1 -16 +(row<<3)) == KNIGHT && isWhite(col+1 -16 +(row<<3)) == whiteIsWhom)
			||  (col>0 && col-1 -16 +(row<<3) > 0 && col-1 -16 +(row<<3) < 63 && pieceType(col-1 -16 +(row<<3)) == KNIGHT && isWhite(col-1 -16 +(row<<3)) == whiteIsWhom)
			||	dangerHelp(col-1,row-1,whiteIsWhom, -1, -1)
			||  dangerHelp(col-1,row  ,whiteIsWhom, -1,  0)
			||  dangerHelp(col-1,row+1,whiteIsWhom, -1,  1)
			||  dangerHelp(col  ,row-1,whiteIsWhom,  0, -1)
			||  dangerHelp(col  ,row+1,whiteIsWhom,  0,  1)
			||  dangerHelp(col+1,row-1,whiteIsWhom,  1, -1)
			||  dangerHelp(col+1,row  ,whiteIsWhom,  1,  0)
			||  dangerHelp(col+1,row+1,whiteIsWhom,  1,  1);
	}
	private boolean dangerHelp(int col, int row, boolean wIW, int dx, int dy){
		int s = col + (row<<3);
		if (col < 0 || col > 7 || row < 0 || row > 7) return false;
		if (isWhite(s) != wIW && pieceType(s) < BLANK0) return false;
		if (isWhite(s) == wIW && pieceType(s) < BLANK0){
			if (dx != 0 && dy != 0 && (pieceType(s) == BISHOP || pieceType(s) == QUEEN)) return true;
			else if((dx == 0 || dy == 0)&& (pieceType(s) == ROOK   || pieceType(s) == QUEEN)) return true;
			else return false;
		}
		return dangerHelp(col+dx, row+dy, wIW, dx, dy);
	}
	
	public boolean isCheckmate(){
		return inDangerFrom(kingCol(), kingRow(), !whiteToMove()) && !hasAnyLegalMove(); 
	}
	public boolean isStalemate(){
		return !inDangerFrom(kingCol(), kingRow(), !whiteToMove()) && !hasAnyLegalMove();
	}
	/*
	 *Returns true if the mover has a legal move.
	 */
	public boolean hasAnyLegalMove(){
		for (int square = 0; square < 64; ++square)
			if (hasLegalMove(square)) return true;
		return false;
	}
	public boolean hasLegalMove(int square){
		int pieceType = pieceType(square);
		boolean wtm = whiteToMove();
		if (	pieceType >= BLANK0
			||	isWhite(square) != wtm) return false;
		if (pieceType == PAWN
				&& !legalMove(square, square+(wtm?8:-8))
				&& !legalMove(square, square+(wtm?16:-16))
				&& !legalMove(square, square+(wtm?9:-9))
				&& !legalMove(square, square+(wtm?7:-7))) return false;
		if (pieceType == KNIGHT
				&& !legalMove(square, square + 10)
				&& !legalMove(square, square + 6)
				&& !legalMove(square, square + 15)
				&& !legalMove(square, square + 17)
				&& !legalMove(square, square - 10)
				&& !legalMove(square, square - 6)
				&& !legalMove(square, square - 15)
				&& !legalMove(square, square - 17)) return false;
		if (pieceType == ROOK) {
			for (int i = 1; i < 9; i++)
				if (legalMove(square, square + i)
				 || legalMove(square, square - i)
				 || legalMove(square, square + 8*i)
				 || legalMove(square, square - 8*i))
					return true;
			
			return false;
		}
		if (pieceType == BISHOP) {
			for (int i = 1; i < 9; i++)
				if (legalMove(square, square + 8*i + i) //diagonal
				 || legalMove(square, square + 8*i - i)
				 || legalMove(square, square - 8*i + i)
				 || legalMove(square, square - 8*i - i))
					return true;
			
			return false;
		}
		if (pieceType == QUEEN) {
			for (int i = 1; i < 9; i++)
				if (legalMove(square, square + i)
				 || legalMove(square, square - i)
				 || legalMove(square, square + 8*i)
				 || legalMove(square, square - 8*i)
				 || legalMove(square, square + 8*i + i) //diagonal
				 || legalMove(square, square + 8*i - i)
				 || legalMove(square, square - 8*i + i)
				 || legalMove(square, square - 8*i - i))
					return true;

			return false;
		}
		if (pieceType == KING
				&& !legalMove(square, square + 1)
				&& !legalMove(square, square - 1)
				&& !legalMove(square, square + 8)
				&& !legalMove(square, square - 8)
				&& !legalMove(square, square + 9)
				&& !legalMove(square, square + 7)
				&& !legalMove(square, square - 9)
				&& !legalMove(square, square - 7)) return false;
		return true;
	}
	
	private static long setBit(long in, int bit, long val){
		val = Math.abs(val);
		in &= ~(1l<<bit);
		in |= val<<bit;
		return in;
	}
	
	/*
	 * Returns true iff the piece at this square is white,
	 * false iff the piece is black.
	 * This includes the blank square colors (which are
	 * not the same as blank square types).
	 */
	public boolean isWhite(int squareNum){
		return (color >>> squareNum)%2 == 0;
	}
	
	public byte pieceType(int squareNum){
		if 		((xxPK_KnBRQ >>> squareNum)%2 != 0){
			if  ((leafTypeGen >>> squareNum)%2 != 0){
				return (leafTypeSpc >>> squareNum)%2 != 0 ? QUEEN : ROOK;
			} else {
				return (leafTypeSpc >>> squareNum)%2 != 0 ? BISHOP : KNIGHT;
			}
		} else {
			if  ((leafTypeGen >>> squareNum)%2 != 0){
				return (leafTypeSpc >>> squareNum)%2 != 0 ? KING : PAWN;
			} else {
				return (leafTypeSpc >>> squareNum)%2 != 0 ? BLANK1 : BLANK0;
			}
		}
	}
	
	/*
	 * Hide meta information in the blank squares.
	 * 1 : 1 : 14 : 2 : 3 : 4 : 7
	 * to move : mover in check : 0 : repetition : en passant : castling rights : 50 move rule
	 */
	public void hideInfo(int info){
		/*long canHoldInfo = ~xxPK_KnBRQ & ~leafTypeGen;
		byte n = 0;
		for (byte square = 0; square < 64; ++square){
			if ( ((canHoldInfo)>>>square)%2 != 0 ) {
				color = setBit(color,square,(info>>>n)%2);
				++n;
				if (n > 31) break;
			}
		}*/
		this.info = info;
	}
	
	/*
	 * Decompress information hidden in blank squares.
	 */
	public int hiddenInfo(){
		/*int info = 0;
		long hasInfo = ~xxPK_KnBRQ & ~leafTypeGen;
		byte nextEmptyBit = 0;
		for (byte bit = 0; bit < 64; ++bit){
			if ((hasInfo>>>bit)%2 == 1 ) {
				info |= (color>>>bit)%2<<nextEmptyBit; //DEBUG
				++nextEmptyBit;
				if (nextEmptyBit > 31) break;
			}
		}*/
		return this.info; /////CHANGE BACK TO JUST INFO TODO
	}
	
	/*
	 * Returns true if this position is the
	 * same as gs. (Including
	 * castling and en passant rights.)
	 *TODO Including same player to move?
	 */
	public boolean positionEquals(GameState gs){
		if (xxPK_KnBRQ != gs.xxPK_KnBRQ) return false;
		if (leafTypeGen != gs.leafTypeGen) return false;
		for (byte square = 0; square < 64; ++square){
			//return false if square is nonempty with different piece colors
			if (isWhite(square) != gs.isWhite(square) && pieceType(square) < BLANK0) return false;
		}
		if (whiteHasCastleKing() != gs.whiteHasCastleKing()) return false;
		if (whiteHasCastleQueen()!= gs.whiteHasCastleQueen())return false;
		if (blackHasCastleKing() != gs.blackHasCastleKing()) return false;
		if (blackHasCastleQueen()!= gs.blackHasCastleQueen())return false;
		if ((hiddenInfo()>>>11)%8!= (gs.hiddenInfo()>>>11)%8)return false;
		return true;
	}
	
	public String stateString() {
		return "\n" + Long.toString(this.color) +
				"\n" +  Long.toString(this.xxPK_KnBRQ) +
				"\n" +  Long.toString(this.leafTypeGen) +
				"\n" +  Long.toString(this.leafTypeSpc);
	}
	
	public boolean moverInCheck(){
		return (hiddenInfo()>>>30)%2 == 1;
	}
	public boolean whiteHasCastleKing(){
		return (hiddenInfo()>>>9)%2 == 1;
	}
	public boolean whiteHasCastleQueen(){
		return (hiddenInfo()>>>10)%2 == 1;
	}
	public boolean blackHasCastleKing(){
		return (hiddenInfo()>>>7)%2 == 1;
	}
	public boolean blackHasCastleQueen(){
		return (hiddenInfo()>>>8)%2 == 1;
	}
	public byte repetitons(){
		return (byte)((hiddenInfo()>>>14)%4);
	}
	public byte fiftyMoveProgress(){
		return (byte)(hiddenInfo()%128);
	}
	public int kingCol(){
		return (whiteToMove()? (hiddenInfo()>>>27)%8 : (hiddenInfo()>>>21)%8);
	}
	public int kingRow(){
		return (whiteToMove()? (hiddenInfo()>>>24)%8 : (hiddenInfo()>>>18)%8);
	}
	public int kingSqr(){
		return (kingRow()<<3) + kingCol();
	}
	public int kingCol(boolean white){
		return (white? (hiddenInfo()>>>27)%8 : (hiddenInfo()>>>21)%8);
	}
	public int kingRow(boolean white){
		return (white? (hiddenInfo()>>>24)%8 : (hiddenInfo()>>>18)%8);
	}
}
